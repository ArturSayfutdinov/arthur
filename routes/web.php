<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/translate', 'PostTranslateController@translate');

//$router->get('translate', function (){
//    $posts=DB::table('dle_post')->get();
//    $posts=\App\Post::all();
//    $posts=\App\Post::getInfoPost();
//    return view('template_translateOfPage', compact('posts'));
//});


