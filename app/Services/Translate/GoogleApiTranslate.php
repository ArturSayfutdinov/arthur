<?php

namespace App\Services\Translate;


use Stichoza\GoogleTranslate\GoogleTranslate;



class GoogleApiTranslate implements TranslateInterface {

    private $translator;
    private $from;
    private $to;
    private $aviable_lang = ['ru-en', 'ru-cn'];

    public function __construct()
    {
        $this->translator = new GoogleTranslate();
    }

    public function setSource($from)
    {
        $this->from = $from;
    }

    public function setTarget($to)
    {
        $this->to = $to;
    }
    public function checkLang()
    {
//        $lang = in_array($this->aviable_lang)
        return $this->from.'-'.$this->to;
    }

    public function langList($lang)
    {

    }
    public function translate($text)
    {
        try {
            $this->translator->trans($text, $this->to, $this->from );
        } catch(\Exception $e) {
            throw new \Exception('Ошибка перевода!'. $e->getMessage());
        }
    }
}