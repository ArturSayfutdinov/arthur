<?php

namespace App\Services\Translate;


use Yandex\Translate\Translator;



class YandexApiTranslate implements TranslateInterface {

	private $translator;
	private $api_key = 'trnsl.1.1.20181206T155032Z.df2259debc694625.7785fe5773277ac3a0359fdb012e0ca82aa28030';
	private $from;
	private $to;
    private $lang_lang;
    private $format = 'html';


	public function __construct()
	{
		$this->translator = new Translator($this->api_key);
	}

	public function setSource($from)
	{
        return $this->from = $from;
	}

	public function setTarget($to)
	{
		$this->to = $to;
	}
    public function checkLang()
    {
        return $this->from.'-'.$this->to;
    }
    public function langList()
    {
        return $this->translator->getSupportedLanguages();

    }



	public function translate($text)
	{
		return $this->translator->translate($text, $this->from.'-'.$this->to );

	}


}
