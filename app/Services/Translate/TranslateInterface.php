<?php

namespace  App\Services\Translate;

interface TranslateInterface{
	public function  translate($text);
}
