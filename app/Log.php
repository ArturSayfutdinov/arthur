<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public const DONE = 'DONE';
    public const ERROR_VALIDATE = 'ERROR_VALIDATE';
    public const ERROR_TRANSLATE = 'ERROR_TRANSLATE';

    public function post()
    {
        return $this->belongsTo('Post');
    }
}
