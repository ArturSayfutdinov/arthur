<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTranslate extends Model
{
    protected $fillable = ['id', 'title', 'short_story', 'full_story', 'lang'];
}
