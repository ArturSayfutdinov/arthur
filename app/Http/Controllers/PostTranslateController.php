<?php

namespace App\Http\Controllers;

use App\Log;
use App\Post;
use App\PostTranslate;
use Illuminate\Http\Request;
use App\Services\Translate\TranslateInterface;
use App\Services\Translate\YandexApiTranslate;

class PostTranslateController extends Controller
{
    public function translate()
    {
        $posts = Post::getInfoPost();

//        dd($posts->toArray());
        $translator = new YandexApiTranslate();
        $lang = "en";

        #для подсчета количества иттераций
        $i=1;

        $translator->setSource('ru');
        $translator->setTarget($lang);


        foreach ($posts as $post) {

                $title = $translator->translate($post->title);
                $full_story = $translator->translate($post->full_story);
                $short_story = $translator->translate($post->short_story);

          if ($post->id<10) {
                $logs = new Log();
                $logs->post_id = $post->id;
                $logs->lang = $lang;
//                $logs->status = 'DONE';
                $logs->save();

                $PostTranslate = new PostTranslate();
                $PostTranslate->title = $title;
                $PostTranslate->short_story = $short_story;
                $PostTranslate->full_story = $full_story;
                $PostTranslate->save();
                print "<li>" . $title . " ===> Готово!</li>";
            }
//        print $i++." -- ".$post->title . " -- " . $post->logs . "<br>";
        }
    }
}
