<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Log;
use App\Post;
use App\PostTranslate;
use Illuminate\Http\Request;
use App\Services\Translate\TranslateInterface;
use App\Services\Translate\YandexApiTranslate;
use Yandex\Translate\Exception;


class Translate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:ya';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запуск перевода статей с помощью яндекс переводчика';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $posts = Post::getInfoPost();

        $translator = new YandexApiTranslate();
        $lang = "en";

        $translator->setSource('ru');
        $translator->setTarget($lang);
        $bar = $this->output->createProgressBar(count($posts));

        foreach ($posts as $post) {

            try {

                if (strlen($post->short_story || $post->full_story) > 10000) {
                    throw new \Exception('Text too long: max 10000');
                }
                $post_id = $post->id;
                $status = Log::DONE;
                $error = 'NO';

                $title = $translator->translate($post->title);
                $full_story = $translator->translate($post->full_story);
                $short_story = $translator->translate($post->short_story);

                $this->recordLog($post_id, $lang, $status, $error);

                $PostTranslate = new PostTranslate();
                $PostTranslate->title = $title;
                $PostTranslate->short_story = $short_story;
                $PostTranslate->full_story = $full_story;
                $PostTranslate->save();


            } catch (\Exception $a) {
                //Устанавливаем статус и ошибку
                $status = Log::ERROR_VALIDATE;
                $error = $a->getCode();

                $this->recordLog($post_id, $lang, $status, $error);
                continue;
            } catch (Yandex\Translate\Exception $e) {
                $status = Log::EROOR_TRANSLATE;
                $error = $a->getCode();

                $this->recordLog($post_id, $lang, $status, $error);
                continue;
            }
            $bar->advance();
        }
    }
    private function recordLog($post_id, $lang, $status, $error)
    {
        $logs = new Log();
        $logs->post_id = $post_id;
        $logs->lang = $lang;
        $logs->status = $status;
        $logs->error = $error;
        $logs->save();
    }
    #Тестовые изменения для Git
    #Еще одно изменение
}
