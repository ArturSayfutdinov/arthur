<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public function logs()
    {
        return $this->hasMany('App\Log');
    }

    #Получаем посты из таблицы posts у которых нет перевода
    public static function getInfoPost()
    {
        return Post::doesntHave("logs")->get();
    }

}
